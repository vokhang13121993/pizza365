import 'bootstrap/dist/css/bootstrap.min.css'
import './style.css'
import ContentComponent from './components/content/ContentComponent';
import FooterComponent from './components/footer/FooterComponent';
import HeaderComponent from './components/header/HeaderComponent';
function App() {
  return (
    <div >
      <HeaderComponent />
      <ContentComponent />
      <div style={{ marginTop: 50 }}>
        <FooterComponent />
      </div>

    </div>
  );
}

export default App;
