
import 'react-pro-sidebar/dist/css/styles.css';
import { Navbar, NavbarBrand, NavItem, Nav,  NavLink } from 'reactstrap';
function SidebarComponent() {
    return (
        <>
            <Navbar>
                <NavbarBrand href='/'>Home</NavbarBrand>
                    <Nav className='me-auto' navbar>
                        <NavItem>
                            <NavLink href='/orders'>
                                Quản lý đơn hàng
                            </NavLink>
                        </NavItem>
                    </Nav>
            </Navbar>
        </>
    )
}
export default SidebarComponent