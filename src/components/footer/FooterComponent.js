import { Col, Row } from "reactstrap"

function FooterComponent(){
    return(
        <>
            <Row className="bg-light text-center footer">
                <Col>
                    <h4>devcamp</h4>
                </Col>
            </Row>
        </>
    )
}
export default FooterComponent