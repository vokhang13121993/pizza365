import axios from "axios"
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap"
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

function ModalDelele({setSwitchModalDelete, switchModalDelete, id}){
    const onClickDelete = () => {
        axios.delete(`http://42.115.221.44:8080/devcamp-pizza365/orders/${id}`)
        .then(data => {
            toast.success('Xóa thành công')
            setTimeout(() => window.location.reload(), 3000)
        })
        .catch(error => {
            toast.error('Xóa thất bại')
        })
    }
    return(
        <>
        <Modal isOpen={switchModalDelete} toggle={() => setSwitchModalDelete(false)}>
            <ModalHeader>Xác nhận lại</ModalHeader>
            <ModalBody>Bạn có thật sự muốn xóa không</ModalBody>
            <ModalFooter>
                <Button className="btn-danger" onClick={onClickDelete}>Confirm</Button>
                <Button className="btn-dark" onClick={() => setSwitchModalDelete(false)}>Cancel</Button>
            </ModalFooter>
        </Modal>
        </>
    )
    
}
export default ModalDelele