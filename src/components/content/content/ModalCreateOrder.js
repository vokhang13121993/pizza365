import { Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Input, Button, Label, Form } from "reactstrap"
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import '../../../style.css'
function ModalCreateOrder({
    switchModalCreate,
    setSwitchModalCreate,
    setCombo,
    setNuocUong,
    objCombo,
    nuocUong,
    loaiPizza,
    createOrder,
    setLoaiPizza,
    switchModalConfirm,
    setSwitchModalConfirm,
    getOrder
}) {

    const onChangeCombo = (event) => {
        setCombo(event.target.value)
    }
    const onChangeNuocUong = (event) => {
        setNuocUong(event.target.value)
    }
    const onChangeLoaiPizza = (event) => {
        setLoaiPizza(event.target.value)
    }
    const onSubmitData = (event) => {
        event.preventDefault()
        let vValiData = validateData(objCombo.kichCo, loaiPizza, nuocUong);
        if (vValiData) {
            let vHoTen = event.target[9].value;
            let vEmail = event.target[10].value;
            let vSoDienThoai = event.target[11].value;
            let vDiaChi = event.target[12].value;
            let vVoucher = event.target[7].value;
            let vLoiNhan = event.target[13].value;

            getOrder(vVoucher, vHoTen, vEmail, vSoDienThoai, vDiaChi, vLoiNhan);
        }
    }

    const validateData = (paramCombo, paramType, paramDrink) => {
        if (paramCombo === "") {
            toast.error('Bạn chưa chọn Combo');
            return false;
        }
        if (paramType === "none") {
            toast.error('Bạn chưa chọn loại pizza');
            return false;
        }
        if (paramDrink === "none") {
            toast.error("Bạn chưa chọn nước uống");
            return false;
        }
        return true;
    }
    return (
        <>
            <Modal isOpen={switchModalCreate} toggle={() => setSwitchModalCreate(false)} size='lg'>
                <Form onSubmit={onSubmitData}>

                    <ModalHeader>Thêm đơn hàng</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col xs='3'>
                                <Label>
                                    <b>Combo<span style={{ color: 'red' }}> (*)</span></b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input type='select' onChange={onChangeCombo}>
                                    <option value={"none"}>Chọn combo</option>
                                    <option value={"S"}>Nhỏ</option>
                                    <option value={"M"}>Vừa</option>
                                    <option value={"L"}>Lớn</option>
                                </Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Đường kính</b>
                            </Col>
                            <Col xs='9'>
                                <Input readOnly value={objCombo.duongKinh} className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Sườn (miếng)</b>
                            </Col>
                            <Col xs='9'>
                                <Input readOnly value={objCombo.suon} className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Salad (gr)</b>
                            </Col>
                            <Col xs='9'>
                                <Input readOnly value={objCombo.salad} className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Số lượng nước uông</b>
                            </Col>
                            <Col xs='9'>
                                <Input readOnly value={objCombo.soLuongNuoc} className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Thành tiền</b>
                            </Col>
                            <Col xs='9'>
                                <Input readOnly value={objCombo.thanhTien} className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Đồ uống<span style={{ color: 'red' }}> (*)</span></b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input type="select" onChange={onChangeNuocUong}>
                                    <option value={"none"}>Chọn 1 nước uống</option>
                                    <option value={"TRATAC"}>Trà tắc</option>
                                    <option value={"TRASUA"}>Trà sữa trân châu</option>
                                    <option value={"LAVIE"}>Lavie</option>
                                    <option value={"PEPSI"}>Pepsi</option>
                                    <option value={"COCA"}>CocaCola</option>
                                    <option value={"FANTA"}>Fanta</option>
                                </Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Voucher</b>
                            </Col>
                            <Col xs='9'>
                                <Input className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Loại pizza<span style={{ color: 'red' }}> (*)</span></b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input type="select" onChange={onChangeLoaiPizza}>
                                    <option value={"none"}>Chọn 1 pizza</option>
                                    <option value={"BACON"}>Bacon</option>
                                    <option value={"HAWAII"}>Hawaii</option>
                                    <option value={"SEAFOOD"}>Seafood</option>
                                </Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Họ tên<span style={{ color: 'red' }}> (*)</span></b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Email</b>
                            </Col>
                            <Col xs='9'>
                                <Input  className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Số điện thoại<span style={{ color: 'red' }}> (*)</span></b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input  className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Địa chỉ<span style={{ color: 'red' }}> (*)</span></b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input  className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Lời nhắn</b>
                            </Col>
                            <Col xs='9'>
                                <Input  className="form-control"></Input>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button className="btn-success" style={{ marginRight: 15 }}>Thêm mới</Button>
                        <Button onClick={() => setSwitchModalCreate(false)} className="btn-dark">Hủy bỏ</Button>
                    </ModalFooter>
                </Form>
            </Modal>
            <Modal isOpen={switchModalConfirm} toggle={() => setSwitchModalConfirm(false)}>
                <ModalHeader>Xác nhận lại</ModalHeader>
                <ModalBody>Bạn có chắc chắn thêm đơn hàng này ?</ModalBody>
                <ModalFooter>
                    <Button onClick={() => createOrder()}>Confirm</Button>
                    <Button onClick={() => setSwitchModalConfirm(false)}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
    )
}
export default ModalCreateOrder