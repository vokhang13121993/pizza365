import { Grid, Pagination, Select, Table, MenuItem, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { Row, Col, Label, Button, Input } from "reactstrap"
import ModalCreateOrder from './ModalCreateOrder'
import ModalUpdateOrder from "./ModalUpdateOrder"
import ModalDelete from "./ModalDelete"
import { useState } from 'react'
function LoadData({
    dataPizza,
    setData,
    filterData,
    switchModalConfirm,
    setSwitchModalConfirm,
    setCombo,
    setNuocUong,
    setLoaiPizza,
    nuocUong,
    loaiPizza,
    createOrder,
    getOrder,
    objCombo
}) {
    const [orderId, setOrderId] = useState(null)
    const [objDetailOrder, setObjDetailOrder] = useState({})
    const [id, setId] = useState(null)
    const [filterTypePizza, setFilter] = useState("")

    const [switchModalCreate, setSwitchModalCreate] = useState(false)
    const [switchModalUpdate, setSwitchModalUpdate] = useState(false)
    const [switchModalDelete, setSwitchModalDelete] = useState(false)

    const onClickCreateOrder = () => {
        setSwitchModalCreate(true)
    }
    const onClickUpdate = (item) => {
        setSwitchModalUpdate(true)
        setOrderId(item.orderId)
    }
    const onClickDelete = (item) => {
        setSwitchModalDelete(true)
        setId(item.id)
    }
    const onChangeType = (event) => {
        setFilter(event.target.value)
    }
    const onClickFilter = () => {
        var vObjResult = filterData.filter(function (item) {
            return (item.loaiPizza.toLowerCase().indexOf(filterTypePizza.toLowerCase()) !== -1)
        });
        setData(vObjResult)
    }
    return (
        <>
            <Row className='text-center'>
                <Col>
                    <h1>Danh sách đơn hàng</h1>
                </Col>
            </Row>
            <Row className="bg-light p-4 mt-4" >
                <Col>
                    <Row><Label><h2>Lọc đơn hàng</h2></Label></Row>
                    <Row className="mt-2">
                        <Col xs='5'>
                            <Row>
                                <Col xs='3'>
                                    <Label>Loại pizza</Label>
                                </Col>
                                <Col xs='9'>
                                    <Input type='select' value={filterTypePizza} onChange={onChangeType}>
                                        <option value={""}>None</option>
                                        <option value={"BACON"}>Bacon</option>
                                        <option value={"HAWAII"}>Hawaii</option>
                                        <option value={"SEAFOOD"}>Seafood</option>
                                    </Input>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs='5'>
                        </Col>
                        <Col xs='2'>
                            <Row>
                                <Col>
                                    <Button onClick={onClickFilter} className="btn-success">Lọc đơn hàng</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="m-4">
                <Col>
                    <Button onClick={onClickCreateOrder} className="btn-success">Thêm đơn hàng</Button></Col>
                <Col></Col>
            </Row>
            <Row className="mt-4">
                <Grid>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center"><b>OrderId</b></TableCell>
                                    <TableCell align="center"><b>Combo</b></TableCell>
                                    <TableCell align="center"><b>Pizza</b></TableCell>
                                    <TableCell align="center"><b>Nước uống</b></TableCell>
                                    <TableCell align="center"><b>Thành tiền</b></TableCell>
                                    <TableCell align="center"><b>Họ Tên</b></TableCell>
                                    <TableCell align="center"><b>SĐT</b></TableCell>
                                    <TableCell align="center"><b>Trạng thái</b></TableCell>
                                    <TableCell align="center"><b>Chi tiết</b></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {dataPizza.map((item, index) => (
                                    <TableRow key={index}>
                                        <TableCell align="center">{item.orderId}</TableCell>
                                        <TableCell align="center">{item.kichCo}</TableCell>
                                        <TableCell align="center">{item.loaiPizza}</TableCell>
                                        <TableCell align="center">{item.idLoaiNuocUong}</TableCell>
                                        <TableCell align="center">{item.thanhTien}</TableCell>
                                        <TableCell align="center">{item.hoTen}</TableCell>
                                        <TableCell align="center">{item.soDienThoai}</TableCell>
                                        <TableCell align="center">{item.trangThai}</TableCell>
                                        <TableCell align="center">
                                            <Button className="btn-warning" style={{ marginRight: 10 }} onClick={() => onClickUpdate(item)}>Update</Button>
                                            <Button className="btn-danger" onClick={() => onClickDelete(item)}>Delete</Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Row>
            <ModalCreateOrder
                switchModalCreate={switchModalCreate}
                setSwitchModalCreate={setSwitchModalCreate}
                setCombo={setCombo}
                setNuocUong={setNuocUong}
                setLoaiPizza={setLoaiPizza}
                nuocUong={nuocUong}
                loaiPizza={loaiPizza}
                createOrder={createOrder}
                getOrder={getOrder}
                switchModalConfirm={switchModalConfirm}
                setSwitchModalConfirm={setSwitchModalConfirm}
                objCombo={objCombo} />
            <ModalUpdateOrder
                switchModalUpdate={switchModalUpdate}
                setSwitchModalUpdate={setSwitchModalUpdate}
                orderId={orderId}
                setObjDetailOrder={setObjDetailOrder}
                objDetailOrder={objDetailOrder}
            />
            <ModalDelete
                switchModalDelete={switchModalDelete}
                setSwitchModalDelete={setSwitchModalDelete}
                id={id} />
        </>
    )
}
export default LoadData