import { Modal, ModalBody, ModalFooter, ModalHeader, Row, Col, Label, Input, Button } from "reactstrap"
import { useEffect, useState } from 'react'
import axios from 'axios'
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
function ModalUpdateOrder({
    switchModalUpdate,
    setSwitchModalUpdate,
    orderId,
    objDetailOrder,
    setObjDetailOrder
}) {
    const [status, setStatus] = useState(null)
    const [id, setId] = useState(null)

    useEffect(() => {
        axios.get(`http://42.115.221.44:8080/devcamp-pizza365/orders/${orderId}`)
            .then(data => {
                setObjDetailOrder(data.data);
                setId(data.data.id)
                setStatus(data.data.trangThai)
            })

            .catch(error => {
                console.log(error)
            })
    }, [orderId]);
    const onChangeStatus = (e) => {
        setStatus(e.target.value)
    }
    const onClickUpdate = () => {
        updateOrder()
    }
    const updateOrder = () => {
        const body = {
            body: {
                trangThai: status
            },
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        axios.put(`http://42.115.221.44:8080/devcamp-pizza365/orders/${id}`, body.body, body.headers)
            .then(data => {
                toast.success('Cập nhật thành công')
                setTimeout(() => window.location.reload(), 3500)
            })
            .catch(error => {
                console.log(error)
                toast.error('Cập nhật thất bại')
            })
    }
    return (
        <>
            <Modal isOpen={switchModalUpdate} toggle={() => setSwitchModalUpdate(false)} size='lg'>
                <ModalHeader>Cập nhật đơn hàng</ModalHeader>
                <ModalBody>
                    <ModalBody>
                        <Row>
                            <Col xs='3'>
                                <Label>
                                    <b>Combo</b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.kichCo} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Đường kính</b>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.duongKinh} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Sườn (miếng)</b>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.suon} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Salad (gr)</b>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.salad} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Số lượng nước uông</b>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.soLuongNuoc} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Thành tiền</b>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.thanhTien} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Đồ uống</b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.idLoaiNuocUong} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Voucher</b>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.idVourcher} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Loại pizza</b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.loaiPizza} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Họ tên</b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.hoTen} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Email</b>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.email} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Số điện thoại</b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.soDienThoai} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <Label>
                                    <b>Địa chỉ</b>
                                </Label>
                            </Col>
                            <Col xs='9'>
                                <Input value={objDetailOrder.diaChi} readOnly></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Lời nhắn</b>
                            </Col>
                            <Col xs='9'>
                                <Input readOnly value={objDetailOrder.loiNhan} className="form-control"></Input>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col xs='3'>
                                <b>Trạng thái</b>
                            </Col>
                            <Col xs='9'>
                                <Input type="select" value={status} onChange={onChangeStatus}>
                                    <option value={"open"}>Open</option>
                                    <option value={"cancel"}>Cancel</option>
                                    <option value={"confirmed"}>Confirmed</option> 
                                </Input>
                            </Col>
                        </Row>
                    </ModalBody>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={onClickUpdate} className='btn-success'>Cập nhật đơn hàng</Button>
                    <Button className='btn-dark' onClick={() => setSwitchModalUpdate(false)}>Hủy bỏ</Button>
                </ModalFooter>
            </Modal>
        </>
    )
}
export default ModalUpdateOrder