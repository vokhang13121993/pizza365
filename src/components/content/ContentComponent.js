
import LoadData from "./content/LoadData"
import Home from "./content/Home"
import { Route, Routes } from "react-router-dom"
import { useState, useEffect } from "react"
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
function ContentComponent() {
    const [vData, setStateData] = useState([])
    const [filterData, setFilterData] = useState([])
    const [switchModalConfirm, setSwitchModalConfirm] = useState(false)

    const [combo, setCombo] = useState('none')
    const [nuocUong, setNuocUong] = useState('none')
    const [loaiPizza, setLoaiPizza] = useState('none')

    const [objCombo, setObjCombo] = useState({
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        thanhTien: "",
        soLuongNuoc: "",
    })
    const [objOrderDetail, setObjOrderDetail] = useState({
        voucher: "",
        hoTen: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    })
    const loadData = async () => {
        axios.get("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                setStateData(data.data)
                setFilterData(data.data)
            })
            .catch((error) => {
                console.log(error)
            })
    }
    useEffect(() => {
        loadData()
        getCombo()
    }, [combo])
    const createOrder = () => {
        const body = {
            body: {
                kichCo: objCombo.kichCo,
                duongKinh: objCombo.duongKinh,
                suon: objCombo.suon,
                salad: objCombo.salad,
                loaiPizza: loaiPizza,
                idVourcher: objOrderDetail.voucher,
                idLoaiNuocUong: nuocUong,
                soLuongNuoc: objCombo.soLuongNuoc,
                hoTen: objOrderDetail.hoTen,
                thanhTien: objCombo.thanhTien,
                email: objOrderDetail.email,
                soDienThoai: objOrderDetail.soDienThoai,
                diaChi: objOrderDetail.diaChi,
                loiNhan: objOrderDetail.loiNhan
            },
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        axios.post('http://42.115.221.44:8080/devcamp-pizza365/orders', body.body, body.headers)
            .then(data => {
                toast.success('Đặt hàng thành công. Mã đơn hàng của bạn là : ' + data.data.orderId);
                setTimeout(() => window.location.reload(), 3500)
            })
            .catch(error => {
                toast.error('Đặt hàng thất bại!!!');
                console.log(error.response);
            })
    }

    const getCombo = () => {
        if (combo == "S") {
            setObjCombo({
                kichCo: "S",
                duongKinh: "20",
                suon: "2",
                salad: "200",
                thanhTien: "150000",
                soLuongNuoc: "2",
            })
        }
        if (combo == "M") {
            setObjCombo({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                thanhTien: "200000",
                soLuongNuoc: "3",
            })
        }
        if (combo == "L") {
            setObjCombo({
                kichCo: "L",
                duongKinh: "30",
                suon: "8",
                salad: "500",
                thanhTien: "250000",
                soLuongNuoc: "4",
            })
        }
    }

    const getOrder = (paramVoucher, paramHoTen, paramEmail, paramSoDienThoai, paramDiaChi, paramLoiNhan) => {
        let valiData = validateData(paramVoucher, paramHoTen, paramEmail, paramSoDienThoai, paramDiaChi)
        if (valiData) {
            setObjOrderDetail({
                voucher: paramVoucher,
                hoTen: paramHoTen,
                email: paramEmail,
                soDienThoai: paramSoDienThoai,
                diaChi: paramDiaChi,
                loiNhan: paramLoiNhan
            })
            setSwitchModalConfirm(true)
        }
    }

    const validateData = (paramVoucher, paramHoTen, paramEmail, paramSoDienThoai, paramDiaChi) => {
        if (paramHoTen === "") {
            toast.error('Bạn phải nhập họ tên');
            return false;
        }
        if (!isNaN(paramHoTen)) {
            toast.error('Tên không được chứa số');
            return false;
        }
        if (paramEmail !== "") {
            if (checkEmail(paramEmail) == false) {
                toast.error('Email sai định dạng');
                return false;
            }
        }
        if (validatePhoneNumber(paramSoDienThoai) == false) {
            toast.error('Bạn phải nhập số điện thoại');
            return false;
        }
        if (paramDiaChi === "") {
            toast.error('Bạn phải nhập địa chỉ');
            return false;
        }
        if (isNaN(paramVoucher)) {
            toast.error('Voucher phải là dạng số');
            return false;
        }
        if (checkVoucher(paramVoucher)) {
            return true;
        }
        return true;
    }

    const checkVoucher = (paramVoucher) => {
        axios.get("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramVoucher)
            .then((data) => {
                toast.success('Áp dụng mã giảm giá thành công, bạn được giảm ' + data.data.phanTramGiamGia + ' %');
            })
            .catch((error) => {
                toast.warning("Không có mã giảm giá này");
            })
    }

    const checkEmail = (paramEmail) => {
        const vRE = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return vRE.test(String(paramEmail).toLowerCase());
    }

    const validatePhoneNumber = (paramPhone) => {
        // Kiểu số điện thoại
        var vTypePhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
        // Test chuỗi nhập vào 
        var vCheck = vTypePhone.test(paramPhone);

        return vCheck;
    }
    return (
        <>
            <Routes>
                <Route exact path="/" element={<Home />} />
                <Route exact path="/orders" element={<LoadData
                    dataPizza={vData}
                    setData={setStateData}
                    filterData={filterData}
                    switchModalConfirm={switchModalConfirm}
                    setSwitchModalConfirm={setSwitchModalConfirm}
                    setCombo={setCombo}
                    setNuocUong={setNuocUong}
                    setLoaiPizza={setLoaiPizza}
                    nuocUong={nuocUong}
                    loaiPizza={loaiPizza}
                    getOrder={getOrder}
                    createOrder={createOrder}
                    objCombo={objCombo}
                />}
                />
            </Routes>
            <ToastContainer />
        </>
    )
}
export default ContentComponent